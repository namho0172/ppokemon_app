import 'package:flutter/material.dart';
import 'package:pokemon_app/components/component_colosseum.dart';
import 'package:pokemon_app/components/component_ppokeomon_item.dart';
import 'package:pokemon_app/model/colosseum_item.dart';

class Colosseum extends StatefulWidget {
  const Colosseum({super.key});

  @override
  State<Colosseum> createState() => _ColosseumState();
}

class _ColosseumState extends State<Colosseum> {
  ColosseumItem ppokemon1 = ColosseumItem('assets/back.gif', '라플라스', 500, 90, 15, 6);
  ColosseumItem ppokemon2 = ColosseumItem('assets/2.gif', '전', 400, 95, 10, 5);

  bool _isTurnPpokemon1 = true; // 기본적으로 1번 몬스터 선빵 1번 몬스터 순서면 2번 몬스터 순서 아님

  bool _ppokemon1Live = true;
  num _ppokemon1CurrentHP = 0;

  bool _ppokemon2Live = true;
  num _ppokemon2CurrentHp = 0;

  String _gameLog = '';

  @override
  void initState(){
    super.initState();
    _calculateFirstTurn(); //몬스터 선빵 판별
    setState(() {
      _ppokemon1CurrentHP = ppokemon1.hp; // 1번 몬스터 잔여 HP (페이지 들어가자마자니깐 최대 체력만큼 )
      _ppokemon2CurrentHp = ppokemon2.hp; // 2번 몬스터 잔여 HP (페이지 들어가자마자니깐 최대 체력만큼 )

    });
  }

  //선빵 누구인지 계산
  void _calculateFirstTurn(){
    if (ppokemon1.speed < ppokemon2.speed) {
      setState(() {
        _isTurnPpokemon1 = false; //
      });
    }
  }

  //떄렸을때 최종 공격력 몇인지 (크리티컬 계산식 넣기)
  num _calculateResultHitPoint(num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // 10% 확률로 크리티컬걸리게 할거다.
    criticalArr.shuffle(); // 섞는다.

    bool isCritical = false; // 기본으로 크리티컬 안터진다.
    if (criticalArr[0] == 1) isCritical = true; // 다 썩은 criticalArr에 0번째 요소가 1이면 크리뜬걸로 한다.

    num resulHit = myHitPower; // 공격력
    if (isCritical) resulHit = resulHit * 2;
    resulHit = resulHit - targetDefPower; // 상대방 방어력만큼 데미지 까주기
    resulHit = resulHit.round(); //반올림 처리

    if (resulHit <= 0) resulHit = 1; // 계산결과 공격이 0이거나 음수일경우 공격포인트 고정 1처리

    return resulHit;
  }

  // 상대 몬스터가 죽었는지 확인하기
  void _checkIsDead(num targetPpokemon) {
    if (targetPpokemon == 1 && (_ppokemon1CurrentHP <= 0)) {
      setState(() {
        _ppokemon1Live = false;
      });
    } else if (targetPpokemon == 2 && (_ppokemon2CurrentHp <= 0)){
      setState(() {
        _ppokemon2Live = false;
      });
    }
  }

  // 공격처리
  void _attPpokemon(num actionPpokemon) {
    num myHitPower = ppokemon1.attack;
    num targetDefPower = ppokemon2.def;

    if (actionPpokemon == 2) {
      myHitPower = ppokemon2.attack;
      targetDefPower = ppokemon1.def;
    }

    num resultHit = _calculateResultHitPoint(
        myHitPower, targetDefPower); //상대 Hp 몇 까야하는지 계

    if (actionPpokemon == 1) { // 공격하는 몬스터가 1번이면 (공격당하는 포켓몬은 2번)
      setState(() {
        _ppokemon2CurrentHp -= resultHit; // 2번 몬스터 체력 깍기
        if (_ppokemon2CurrentHp <= 0) _ppokemon2CurrentHp = 0; //체력 0미만이면 0으로 고정
        _checkIsDead(2); //2번 몬스터 죽었는지 확인
      });
    } else {
      setState(() {
        _ppokemon1CurrentHP -= resultHit; //1번 몬스터 체력 깍기
        if (_ppokemon1CurrentHP <= 0) _ppokemon1CurrentHP = 0; //체력 0미만이면 0으로 고정
        _checkIsDead(1); //1번 몬스터 죽었는지 확인
      });
    }
    setState(() {
      _isTurnPpokemon1 = !_isTurnPpokemon1;
    });
  }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('배틀'),
        ),
        body: _buildBody(context),
      );
  }
  Widget _buildBody(BuildContext context){
    return SingleChildScrollView(
      child: Container(
        child: Row(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 300, 0, 0),
                    child: Row(
                      children: [
                        ComponentColosseum(
                            colosseumItem: ppokemon1,
                            callback: () {
                              _attPpokemon(1);
                            },
                            isMuTurn: _isTurnPpokemon1,
                            isLive: _ppokemon1Live,
                            currentHp: _ppokemon1CurrentHP,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                children: [
                  ComponentColosseum(
                      colosseumItem: ppokemon2,
                      callback: () {
                        _attPpokemon(2);
                      },
                      isMuTurn: !_isTurnPpokemon1,
                      isLive: _ppokemon2Live,
                      currentHp: _ppokemon2CurrentHp,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

