import 'package:flutter/material.dart';
import 'package:pokemon_app/components/component_ppokeomon_item.dart';
import 'package:pokemon_app/model/ppokemon_item.dart';
import 'package:pokemon_app/repository/repo_ppokemon.dart';

class PpokemonList extends StatefulWidget {
  const PpokemonList({super.key});

  @override
  State<PpokemonList> createState() => _PpokemonListState();
}

class _PpokemonListState extends State<PpokemonList> {
  List<PpokemonItem> _list = [];

  Future<void> _loadList()  async {
    // 이 메서드가 repo 호출해서 받아온 다음에..
    // setstate해서 _list 교체 할거다
    await RepoPpokemon().getProduct()
        .then((res) =>  {
          print(res),
          setState(() {
            _list = res.list;
          })
    })
    .catchError((err)=> {
      debugPrint(err)
    } )
    ;
  }

  @override
  void initState(){
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('뽀켓몬'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext context, int idx) {
            return ComponentPpokemonItem(ppokemonItem: _list[idx], callback: () {});
          },
        ),
      ),
    );
  }
}