import 'package:dio/dio.dart';
import 'package:pokemon_app/config/config_api.dart';
import 'package:pokemon_app/model/ppokemon_list_result.dart';
class RepoPpokemon {
  Future<PpokemonListResult> getProduct() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/ppokemon/all'; // 엔드포인트


    final response = await dio.get(
        _baseUrl,
        options:Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
        )
    );

    
    return PpokemonListResult.fromJson(response.data);
  }
}