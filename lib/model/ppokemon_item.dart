class PpokemonItem {
  num id;
  String ppokemonName;
  String imgUrl;
  String elemental;
  num hp;
  num attack;
  num defense;
  num speed;
  num rank;
  String introduction;
  //백엔드에서 받아온 리스트를 그릇에 담았어요

  PpokemonItem(this.id, this.ppokemonName, this.imgUrl, this.elemental,
      this.hp, this.attack, this.defense, this.speed, this.rank, this.introduction);
  //생성자 (class에는 항상 필요함)

  factory PpokemonItem.fromJson(Map<String, dynamic> json){
    // PpokemonItem.fromJson . 기준으로 앞에  Ppokemon : 리턴타입 fromJson 메소드에 이름
    //즉 Json을 받아서 PpokemonItem으로 바꿔주고 이름을 fromJson으로 하겠다
    return PpokemonItem(
      json['id'], // json의 아이디 ~~
      json['ppokemonName'],
      json['imgUrl'],
      json['elemental'],
      json['hp'],
      json['attack'],
      json['defense'],
      json['speed'],
      json['rank'],
      json['introduction'],
    );
  }
}