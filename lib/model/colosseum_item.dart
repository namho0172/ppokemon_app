
class ColosseumItem {
  String imgUrl;
  String ppokemonName;
  num hp;
  num attack;
  num def;
  num speed;

  ColosseumItem(this.imgUrl, this.ppokemonName, this.hp, this.attack, this.def, this.speed);
}