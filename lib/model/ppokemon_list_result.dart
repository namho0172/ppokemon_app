import 'package:pokemon_app/model/ppokemon_item.dart';

class PpokemonListResult {
  String msg;
  num code;
  List<PpokemonItem> list; //PpokemonItem 의 리스트
  num totalCount;

  PpokemonListResult(this.msg, this.code, this.list, this.totalCount);
  //컴파일 언어는 타입이 존재하기 때문에 이런 과정을 거쳐야 해요

  factory PpokemonListResult.fromJson(Map<String,dynamic>json){
    return PpokemonListResult(
      json ['msg'],
      json ['code'],
      json ['list'] !=null ? (json['list'] as List).map((e) => PpokemonItem.fromJson(e)).toList() :[],
      json ['totalCount']
    );
    // json으로 [{name : 피카츄 }, {name : 구구}] 이런식으로 String으로 받아서 Object의 list로 바꾸고 난 후에
    // 그럼 어쩃든 List니까 한뭉탱이씩 던져 줄수 있다.
    // 근데.. 한뭉탱이씩 더지겠다 하면서 한 뭉탱이의 이름이 바로 e
    // 다 작은 그릇으로 바꿔치기 한다음에
    // 다시 그것들을 싹 가지고 와서 리스트로 촥하고 정리해줌
  }
}