import 'package:flutter/material.dart';
import 'package:pokemon_app/model/colosseum_item.dart';
import 'package:pokemon_app/model/ppokemon_item.dart';

class ComponentColosseum extends StatefulWidget {
  const ComponentColosseum({
    super.key,
    required this. colosseumItem,
    required this.callback,
    required this.isMuTurn,
    required this.isLive,
    required this.currentHp
  });

  final ColosseumItem colosseumItem;
  final VoidCallback callback;
  final bool isMuTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentColosseum> createState() => _ComponentColosseumState();
}

class _ComponentColosseumState extends State<ComponentColosseum> {
  double _calculateHpPercent() {
    return widget.currentHp / widget.colosseumItem.hp;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(
              '${widget.isLive ? widget.colosseumItem.imgUrl : 'assets/back.gif'}',
              fit: BoxFit.fill,
            ),
          ),
          Text(widget.colosseumItem.ppokemonName),
          Text('HP ${widget.colosseumItem.hp}'),
          SizedBox(
            width: MediaQuery.of(context).size.width/2,
            child: LinearProgressIndicator(
              value: _calculateHpPercent(),
            ),
          ),
          (widget.isMuTurn && widget.isLive) ?
              OutlinedButton(onPressed: widget.callback, child: Text('공격'),
              ) : Container(),
        ],
      ),
    );
  }
}

