import 'package:flutter/material.dart';
import 'package:pokemon_app/model/ppokemon_item.dart';

class ComponentPpokemonItem extends StatelessWidget {
  const ComponentPpokemonItem(
      {super.key, required this.ppokemonItem, required this.callback});

  final PpokemonItem ppokemonItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Column(
              children: [
                Image.asset(ppokemonItem.imgUrl),
                Text(ppokemonItem.ppokemonName),
                Text(ppokemonItem.elemental),
                Text('HP ${ppokemonItem.hp}'),
                Text('ATK ${ppokemonItem.attack}'),
                Text('DFE ${ppokemonItem.defense}'),
                Text('SPD ${ppokemonItem.speed}'),
                Text('RANK ${ppokemonItem.rank}'),
                Text(ppokemonItem.introduction)
              ],
            )
          ],
        ),
      ),
    );
  }
}
